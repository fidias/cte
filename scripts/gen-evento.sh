#!/usr/bin/env bash

set -o nounset
set -o errexit
set -o pipefail

xjc -d src/main/java/ \
    -p br.inf.portalfiscal.cte.evento \
    schemas/eventoCTe_v3.00.xsd \
    schemas/evCancCTe_v3.00.xsd
