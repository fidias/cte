#!/usr/bin/env bash

set -o nounset
set -o errexit
set -o pipefail

xjc -d src/main/java/ \
    -p br.inf.portalfiscal.cte \
    schemas/cte_v3.00.xsd \
    schemas/cteModalRodoviario_v3.00.xsd \
    schemas/cancCTeTiposBasico_v3.00.xsd \
    schemas/consReciCTe_v3.00.xsd
