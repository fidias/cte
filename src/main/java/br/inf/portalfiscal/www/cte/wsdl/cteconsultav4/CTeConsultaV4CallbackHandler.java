
/**
 * CTeConsultaV4CallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.6  Built on : Aug 30, 2011 (10:00:16 CEST)
 */

    package br.inf.portalfiscal.www.cte.wsdl.cteconsultav4;

    /**
     *  CTeConsultaV4CallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class CTeConsultaV4CallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public CTeConsultaV4CallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public CTeConsultaV4CallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for cteConsultaCT method
            * override this method for handling normal response from cteConsultaCT operation
            */
           public void receiveResultcteConsultaCT(
                    br.inf.portalfiscal.www.cte.wsdl.cteconsultav4.CTeConsultaV4Stub.CteConsultaCTResult result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from cteConsultaCT operation
           */
            public void receiveErrorcteConsultaCT(java.lang.Exception e) {
            }
                


    }
    