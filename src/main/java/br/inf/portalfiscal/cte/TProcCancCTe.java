//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.10.09 às 09:03:50 AM BRT 
//


package br.inf.portalfiscal.cte;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Tipo Pedido de Cancelamento de CT-e processado
 * 
 * <p>Classe Java de TProcCancCTe complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TProcCancCTe">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cancCTe" type="{http://www.portalfiscal.inf.br/cte}TCancCTe"/>
 *         &lt;element name="retCancCTe" type="{http://www.portalfiscal.inf.br/cte}TRetCancCTe"/>
 *       &lt;/sequence>
 *       &lt;attribute name="versao" use="required" type="{http://www.portalfiscal.inf.br/cte}TVerCancCTe" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TProcCancCTe", propOrder = {
    "cancCTe",
    "retCancCTe"
})
public class TProcCancCTe {

    @XmlElement(required = true)
    protected TCancCTe cancCTe;
    @XmlElement(required = true)
    protected TRetCancCTe retCancCTe;
    @XmlAttribute(name = "versao", required = true)
    protected String versao;

    /**
     * Obtém o valor da propriedade cancCTe.
     * 
     * @return
     *     possible object is
     *     {@link TCancCTe }
     *     
     */
    public TCancCTe getCancCTe() {
        return cancCTe;
    }

    /**
     * Define o valor da propriedade cancCTe.
     * 
     * @param value
     *     allowed object is
     *     {@link TCancCTe }
     *     
     */
    public void setCancCTe(TCancCTe value) {
        this.cancCTe = value;
    }

    /**
     * Obtém o valor da propriedade retCancCTe.
     * 
     * @return
     *     possible object is
     *     {@link TRetCancCTe }
     *     
     */
    public TRetCancCTe getRetCancCTe() {
        return retCancCTe;
    }

    /**
     * Define o valor da propriedade retCancCTe.
     * 
     * @param value
     *     allowed object is
     *     {@link TRetCancCTe }
     *     
     */
    public void setRetCancCTe(TRetCancCTe value) {
        this.retCancCTe = value;
    }

    /**
     * Obtém o valor da propriedade versao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersao() {
        return versao;
    }

    /**
     * Define o valor da propriedade versao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersao(String value) {
        this.versao = value;
    }

}
