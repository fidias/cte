# CT-e - Conhecimento de Transporte Eletrônico

Projeto para geração e compilação das Classes Java
referentes ao projeto CT-e.

Este projeto utiliza Maven2.

## XSD - Passo a passo

Versão do org.apache.axis2.axis2-adb: 1.5.6

Baixar arquivos de <http://www.cte.fazenda.gov.br/portal/listaConteudo.aspx?tipoConteudo=0xlG1bdBass=>.

Substituir arquivos baixados no diretório `schemas`.

Num terminal executar:

### Geral:

```
xjc -d src/main/java/ \
    -p br.inf.portalfiscal.ctev4 \
    schemas/v4/cte_v4.00.xsd \
    schemas/v4/cteModalRodoviario_v4.00.xsd
```

### Eventos:

```
xjc -d src/main/java/ \
    -p br.inf.portalfiscal.ctev4.evento \
    schemas/v4/eventoCTe_v4.00.xsd \
    schemas/v4/evCancCTe_v4.00.xsd \
    schemas/v4/consSitCTeTiposBasico_v4.00.xsd
```

## WSDL

## Passo a passo

```
~/dev-lib/java/axis2-1.5.6/bin/wsdl2java.sh -S src/main/java/ -uri wsdl/v4/CTeRecepcaoSincV4.wsdl
~/dev-lib/java/axis2-1.5.6/bin/wsdl2java.sh -S src/main/java/ -uri wsdl/v4/CTeConsultaV4.wsdl
~/dev-lib/java/axis2-1.5.6/bin/wsdl2java.sh -S src/main/java/ -uri wsdl/v4/CTeRecepcaoEventoV4.wsdl
```
